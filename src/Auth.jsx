import React, { useState} from 'react';
import firebase from "firebase/app";
import "firebase/auth";

var user='empty';
let styles = {
    margin: 'auto',
    width: '50%',
    border: '8px solid green',
    padding: '100px',
};
let styless = {
    float: 'right',
};
const Apl= (props)=>{

    const [email,setEmail]=useState('');
    const [password,setPassword]=useState('');
    user=firebase.auth().currentUser;
    console.log(user);

    const submit= async ()=>{
    await firebase.auth().createUserWithEmailAndPassword(email,password);
       console.log('usuario creado');
       user= firebase.auth().currentUser;
       alert('Usuario Creado: '+user.email);

    }
    const login= async ()=>{
        await firebase.auth().signInWithEmailAndPassword(email,password).then((userCredential) => {
            // Signed in
            console.log('logueado');
            user= firebase.auth().currentUser;
            alert('Bienvenido: '+user.email);
          })
          .catch((error) => {
           // var errorCode = error.code;
            var errorMessage = error.message;
            alert('Error: '+errorMessage);
          });     
    }
    const logout=async()=>{
        await   firebase.auth().signOut();
        console.log('Sesión cerrada');
        //return user;

    }
       

    return (
        <div style   ={styles} >         
            {  
                !user &&
                <div >
                    {/* <form >*/}
                    <div >
                    <label htmlFor="email">Correo Electrónico: </label>  
                        <input type="email" id="email"  required  onChange={  (ev)=> setEmail(ev.target.value)}/>                        
                    </div>
                    <br/>
                    <div >
                        <label htmlFor="password">Contraseña: </label>
                        <input type="password" minLength ="6" required id="password"  onChange={(ev)=>setPassword(ev.target.value)}/>
                    </div>
                    <br/>
                    <div>     
                         <button className="btn btn-primary" onClick = {login}>  Iniciar Sesión</button>
                         <button style ={styless} className="btn btn-success" onClick = {submit}>  Crear Cuenta</button>
                   </div>   
                   {/* </form>*/}
                   

                </div>                
            }            
            {
               user && 
                /*<Logout/>*/
                <div>
                    <p>Usuario: {user.email}</p>
                    <button className="btn btn-danger" onClick={logout}>Cerar Cesión </button>
                </div>             
            }             
        </div>
    )    
}
export default Apl;